create database diproma

use diproma

create table categoriaProducto
(
	idCategoria int primary key not null,
	nombre varchar(50)
)

create table producto
(
	idProducto int primary key not null,
	nombre varchar(50),
	precio int,
	idCategoria int,
	cantidadPro int
	constraint FK_Categoria foreign key (idCategoria) references categoriaProducto(idCategoria)
)

create table Carrito
(
	idCarrito int not null,
	nombre varchar(25),
	detalle varchar(50),
	idCliente int,
	idPro int,
	cantidad int,
	subtotal int
	constraint FK_User foreign key (idCliente) references cliente(NIT),
	constraint FK_Prod foreign key (idPro) references producto(idPro),
)

create table metodoPago
(
	id int primary key,
	nombre varchar(15)
)

create table orden
(
	idOrden int primary key not null,
	idUsuario int,
	idMetodoPago int,
	total int,
	estado bit
)

create table ciudad
(
	idCiudad int primary key not null,
	nombre varchar(15)
)

create table depto
(
	idDepto int primary key not null,
	nombre varchar(15),
	idCiuda int
	constraint FK_ciudad foreign key (idCiuda) references ciudad(idCiudad)
)

create table cliente
(
	NIT int primary key not null,
	nombre varchar(20),
	apellido varchar(20),
	fechaNac date,
	direcDomi varchar(70),
	telDom int,
	telCel int,
	email varchar (50),
	ciudad int,
	depto int,
	limite_credito float,
	dias_credito int
	constraint FK_CiudC foreign key (ciudad) references ciudad(idCiudad),
	constraint FK_DeptC foreign key (depto) references depto(idDepto)
)

create table puestoEmpleado
(
	codigo int primary key not null,
	nombre varchar(15) not null
)

create table empleado
(
	NIT varchar(10) primary key not null,
	nombre varchar(20),
	apellido varchar(20),
	fechaNac date,
	direcDomi varchar(70),
	telDom int,
	telCel int,
	email varchar (50),
	puesto int,
	codigoJefe int,
	pass varchar(20),
	constraint FK_puesto foreign key (puesto) references puestoEmpleado(codigo),
)

create table vehiculo
(
	idVehiculo int primary key,
	mMotor int,
	chasis int,
	marca varchar(15),
	estilo varchar(20),
	modelo varchar(20),
	kilometraje int
)










 --0 es admin
 --1 es gerente
 --2 es supervisor
 --3 es vendedor

insert into EMPLEADO values('201709003', 'Pablo', 'Ayap�n', '09/09/1997', '8 avenida', 24334842, 58284132, 'ayapan.pab009@outlook.es', 'admin',0);
insert into EMPLEADO values('201847853', 'Vielka', 'Aguilar', '01/05/2000', '7 calle', 24342135, 50154783, 'lecorreo@outlook.es', 'yk',1);
insert into EMPLEADO values('214731401', 'Heyy', 'Youu', '11/01/1994', '1 avenida', 24357401, 54574010, 'leemail009@outlook.com', 'jelouda',2);
insert into EMPLEADO values('900123', 'Josu�', 'Vargas', '10/09/1995', '8 calle', 24334782, 58954132, 'yatusabanas009@gmail.com', 'leclave',3);

DELETE FROM empleado;
DELETE FROM puestoEmpleado;

select * from producto;

select * from empleado;
select * from puestoEmpleado;