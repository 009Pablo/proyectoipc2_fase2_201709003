﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;

namespace Proyecto1.SQL
{
    public class Agregar
    {
        SqlConnection conectar = Conexion.RecibirConexion();
        public void insertarEmpleado(string nit, string nombre, string apellido, string fecha, string direcc, int telefono, int celular, string email, int puesto, string codJefe, string contra)
        {
            try
            {
                //abrir conexion
                conectar.Open();
                //insertar empleado
                String agregar = "INSERT INTO empleado VALUES(@a, @b, @c, @d, @e, @f, @g, @h, @i, @j, @k);";
                SqlCommand comando = new SqlCommand(agregar, conectar);

                comando.Parameters.AddWithValue("@a", nit);
                comando.Parameters.AddWithValue("@b", nombre);
                comando.Parameters.AddWithValue("@c", apellido);
                comando.Parameters.AddWithValue("@d", fecha);
                comando.Parameters.AddWithValue("@e", direcc);
                comando.Parameters.AddWithValue("@f", telefono);
                comando.Parameters.AddWithValue("@g", celular);
                comando.Parameters.AddWithValue("@h", email);
                comando.Parameters.AddWithValue("@i", puesto);
                comando.Parameters.AddWithValue("@j", codJefe);
                comando.Parameters.AddWithValue("@k", contra);

                try
                {
                    comando.ExecuteScalar();
                    conectar.Close();
                }
                catch(Exception e)
                {
                    conectar.Close();
                }

            }
            catch(Exception e)
            {
                conectar.Close();
            }
        }

        public void insertarPuesto(int codigo, string nombre)
        {
            try
            {
                //abrir conexion
                conectar.Open();
                //insertar empleado
                String agregar = "INSERT INTO puestoEmpleado VALUES(@a, @b);";
                SqlCommand comando = new SqlCommand(agregar, conectar);

                comando.Parameters.AddWithValue("@a", codigo);
                comando.Parameters.AddWithValue("@b", nombre);

                try
                {
                    comando.ExecuteScalar();
                    conectar.Close();
                }
                catch (Exception e)
                {
                    conectar.Close();
                }

            }
            catch (Exception e)
            {
                conectar.Close();
            }
        }

    }
}