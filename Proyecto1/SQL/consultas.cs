﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Proyecto1.SQL
{
    public class consultas
    {
        //crear objeto de tipo conexión
        SqlConnection conectar = Conexion.RecibirConexion();
        
        public String verificarPass(string usuario)
        {
            try
            {
                //abrir conexion
                conectar.Open();
                String consulta = "SELECT pass FROM empleado WHERE NIT = @usuario";
                //crear objeto de tipo SqlCommand y el parámetro sera el String
                SqlCommand comando = new SqlCommand(consulta, conectar);

                //Para agregar el parametro al WHERE usuario = @usuario
                comando.Parameters.AddWithValue("@usuario", usuario);

                try
                {
                    string contra = (comando.ExecuteScalar()).ToString();
                    conectar.Close();
                    return contra;
                }
                catch(Exception e)
                {
                    conectar.Close();
                    return null;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public String verificarTipoEmpleado(string usuario)
        {
            try
            {
                //abrir conexion
                conectar.Open();
                String consulta = "SELECT puesto FROM empleado WHERE NIT = @usuario";
                //crear objeto de tipo SqlCommand y el parámetro sera el String
                SqlCommand comando = new SqlCommand(consulta, conectar);

                //Para agregar el parametro al WHERE usuario = @usuario
                comando.Parameters.AddWithValue("@usuario", usuario);

                try
                {
                    string tipoEmpleado = (comando.ExecuteScalar()).ToString();
                    conectar.Close();
                    return tipoEmpleado;
                }
                catch (Exception e)
                {
                    conectar.Close();
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

    }
}