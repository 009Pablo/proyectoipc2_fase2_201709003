﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Proyecto1.SQL
{
    public class Conexion
    {
        //conexión
        private static SqlConnection conexion = new SqlConnection("Data source=(localdb)\\MSSQLLocalDB; Initial Catalog=diproma; Integrated Security=True;");

        //recibir la conexión
        public static SqlConnection RecibirConexion()
        {
            return conexion;
        }

        //para SQLCommands
        public SqlCommand AgregarComando(string instrucciones)
        {
            SqlCommand comando = new SqlCommand(instrucciones, conexion);
            return comando;
        }
    }
}