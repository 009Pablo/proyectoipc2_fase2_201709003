﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistroPersona.aspx.cs" Inherits="Proyecto1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Registro</title>
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <h5 class="my-0 mr-md-auto font-weight-normal" style="cursor: default">DIPROMA - Registro Cliente</h5>
            <nav class="my-2 my-md-0 mr-md-3">
                <a class="p-2 text-dark" href="/Default">Inicio</a>
                <a class="p-2 text-dark" href="/RegistroEmpresa">Registrar como Empresa</a>
            </nav>
        </div>
        <div align="center">
            <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
            <asp:Label ID="Label4" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox1" runat="server" Width="346px"></asp:TextBox>
            <br />
            <asp:Label ID="Label5" runat="server" Text="Apellido"></asp:Label>
            <asp:Label ID="Label6" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox2" runat="server" Height="29px" Width="348px"></asp:TextBox>
            <br />
            <asp:Label ID="Label7" runat="server" Text="Correo Eléctrónico"></asp:Label>
            <asp:Label ID="Label8" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;
            <asp:TextBox ID="TextBox3" runat="server" Width="347px"></asp:TextBox>
            <br />
            <asp:Label ID="Label9" runat="server" Text="Fecha de Nacimiento"></asp:Label>
            <asp:Label ID="Label10" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;&nbsp;
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label11" runat="server" Text="Teléfono Domicilio"></asp:Label>
            <asp:Label ID="Label12" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <br />
            Teléfono móvil&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <br />
            NIT<asp:Label ID="Label13" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label14" runat="server" Text="Municipio"></asp:Label>
            <asp:Label ID="Label15" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label16" runat="server" Text="Departamento"></asp:Label>
            <asp:Label ID="Label17" runat="server" ForeColor="#E30000" Text="*"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Registrarse" />
            <br />


        </div>
        <div class="container body-content">
            <hr />
            <footer>
                <p style="cursor: default">
                    &copy; - Distribuidora de Productos Magníficos, S.A.</p>
            </footer>
        </div>
    </form>
    
</body>
</html>
