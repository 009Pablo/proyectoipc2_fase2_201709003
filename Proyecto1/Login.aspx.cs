﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Proyecto1.SQL;
using Proyecto1.XML;

namespace Proyecto1
{
    public partial class Login : System.Web.UI.Page
    {
        consultas leConsulta = new consultas();
        Lectura leLectura = new Lectura();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String user = Convert.ToString(txtUser.Text);
            string pass = Convert.ToString(txtPass.Text);

            try
            {
                String lePass = leConsulta.verificarPass(user);
                String leTipo = leConsulta.verificarTipoEmpleado(user);

                if (lePass.Equals(pass))
                {
                    switch (leTipo)
                    {
                        case "0":
                            Response.Redirect("AdministradorHome.aspx");
                            break;
                        case "3":
                            Response.Redirect("VendedorHome.aspx");
                            break;
                    }
                }
                else
                {
                    Response.Write("<script>window.alert('Contraseña Incorrectos.');</script>");
                    txtPass.Text = "";
                }
            }
            catch
            {
                Response.Write("<script>window.alert('Nombre Usuario no existe.');</script>");
                txtUser.Text = "";
                txtPass.Text = "";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            leLectura.infoXML();
        }
    }

}