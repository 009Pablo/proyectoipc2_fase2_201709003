﻿using Proyecto1.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Xml;

namespace Proyecto1.XML
{
    public class Lectura
    {
        Agregar leAgregar = new Agregar();
        public void infoXML()
        {
            XmlDocument doc;
            doc = new XmlDocument();

            doc.Load("D://Usuarios//ayapa//Escritorio//U//2020//1 vacas//IPC2//Proyecto//leDatos1.xml");

            ////leer etiqueta puesto
            XmlNodeList puesto = doc.SelectNodes("definicion/puesto");
            XmlNode lePuesto;

            for (int i = 0; i < puesto.Count; i++)
            {
                lePuesto = puesto.Item(i);

                int cod = Int32.Parse(lePuesto.SelectSingleNode("codigo").InnerText);
                string nom = lePuesto.SelectSingleNode("nombre").InnerText;

                leAgregar.insertarPuesto(cod, nom);
            }

            //leer etiqueta empleado
            XmlNodeList empleado = doc.SelectNodes("definicion/empleado");
            XmlNode unEmpleado;

            for (int i = 0; i < empleado.Count; i++)
            {
                unEmpleado = empleado.Item(i);

                string nit = unEmpleado.SelectSingleNode("NIT").InnerText;
                string nom = unEmpleado.SelectSingleNode("nombres").InnerText;
                string ape = unEmpleado.SelectSingleNode("apellidos").InnerText;
                string date = unEmpleado.SelectSingleNode("nacimiento").InnerText;
                string direc = unEmpleado.SelectSingleNode("direccion").InnerText;
                int tel = Int32.Parse(unEmpleado.SelectSingleNode("telefono").InnerText);
                int cel = Int32.Parse(unEmpleado.SelectSingleNode("celular").InnerText);
                string email = unEmpleado.SelectSingleNode("email").InnerText;
                int codi = Int32.Parse(unEmpleado.SelectSingleNode("codigo_puesto").InnerText);
                string codiJefe = unEmpleado.SelectSingleNode("codigo_jefe").InnerText;
                string pas = unEmpleado.SelectSingleNode("pass").InnerText;
                

                leAgregar.insertarEmpleado(nit, nom, ape, date, direc, tel, cel, email, codi, codiJefe, pas);
            }
        }


    }
}